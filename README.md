# Gitlab.Patroni

Deploy Patroni HA-Cluster
  1. HAProxy - Load Balancer
  2. etcd - DCS for Patroni
  3. Patroni - Manage and configure Postgres
  4. PostgreSQL

https://gitlab.com/infra-e32/gitlab.patroni